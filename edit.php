<?php
include_once('lib/application.php');

$id=$_GET['id'];
$query="SELECT * FROM tbl_city WHERE id=". $id;
$result=  mysqli_query($link, $query);
$row=  mysqli_fetch_assoc($result);
$data=$row;
$optDhaka="";
$optRajshahi="";
$optComilla="";
$optChittagang="";
$optPabna="";
$optBarishal="";

if($data['city']=='Dhaka'){
    $optDhaka="Selected=selected";
}
if($data['city']=='Rajshahi'){
    $optRajshahi="Selected=selected";
}
if($data['city']=='Comilla'){
    $optComilla="Selected=selected";
}
if($data['city']=='Chittagang'){
    $optChittagang="Selected=selected";
}
if($data['city']=='Pabna'){
    $optPabna="Selected=selected";
}
if($data['city']=='Barishal'){
    $optBarishal="Selected=selected";
}
?>

<form action="store.php?id=<?= $id ?>" method="post">
<fieldset>
    <legend>City Information</legend>
    <div>
        <label for="txtFullName">Full Name:</label>
        <input type="text" name="full_name" id="txtFullName" value="<?= $data['full_name'] ?>" />
    </div>
    <div>
        <label>City:</label>
        <select name="city" id="optCity">
            <option>Please Select City .....</option>
            <option value="Dhaka" <?php echo $optDhaka; ?>>Dhaka</option>
            <option value="Rajshahi" <?php echo $optRajshahi; ?>>Rajshahi</option>
            <option value="Comilla" <?php echo $optComilla; ?>>Comilla</option>
            <option value="Chittagang" <?php echo $optChittagang; ?>>Chittagang</option>
            <option value="Pabna" <?php echo $optPabna; ?>>Pabna</option>
            <option value="Barishal" <?php echo $optBarishal; ?>>Barishal</option>
        </select>
        <br /><input type="Submit" value="Save Information">
    </div>
</fieldset>
    </form>
<nav>
    <li><a href="create.php">Add More</a></li>
    <li><a href="index.php">List</a></li>
    <li><a href="show.php?id=<?= $id ?>">Show</a></li>
    <li><a href="delete.php?id=<?= $id ?>">Delete</a></li>
</nav>